<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>register</title>
  <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="register/css/login.css">
</head>
<body>
  <main>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6 login-section-wrapper">
          <div class="brand-wrapper">
            <img src="register/images/logo.svg" alt="logo" class="logo">
          </div>
          <div class="login-wrapper my-auto">
            <h1 class="login-title">Log in</h1>
            <form action="register" method="post">
              <div class="form-group">
                <label for="username">Username</label>
                <span style="color:red;">${usernames}</span>
                <span style="color:red;">${user}</span>
                <input type="text" name="username" id="username" class="form-control" placeholder="enter your username">
              </div>
                <div class="form-group">
                <label for="dob">DOB</label>
                <span style="color:red;">${dobs}</span>
                <input type="date" name="dob" id="username" class="form-control" >
              </div>
                <div class="form-group">
                <label for="phone">Phone</label>
                <span style="color:red;">${phones}</span>
                <span style="color:red;">${phonee}</span>
                <input type="text" name="phone" id="username" class="form-control" placeholder="enter your phone">
              </div>
              <div class="form-group mb-4">
                <label for="password">Password</label>
                <span style="color:red;">${passs}</span>
                <input type="password" name="pass" id="password" class="form-control" placeholder="enter your passsword">
              </div>
                <div class="form-group mb-4">
                <label for="repassword">RePassword</label>
                <span style="color:red;">${repasss}</span>
                <span style="color:red;">${notice}</span>
                <input type="password" name="repass" id="password" class="form-control" placeholder="enter your repasssword">
              </div>
              <input name="login" id="login" class="btn btn-block login-btn" type="submit" value="sigup!" style = "background-color: #80ced6;">
            </form>
          </div>
        </div>
        <div class="col-sm-6 px-0 d-none d-sm-block">
          <img src="register/images/login.jpg" alt="login image" class="login-img">
        </div>
      </div>
    </div>
  </main>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>
