<%@page import="java.util.List"%>
<%@page import="Model.Lesson"%>
<%@page import="DAO.LessonDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Course Details</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Unicat project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
        <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
        <link rel="stylesheet" type="text/css" href="styles/course.css">
        <link rel="stylesheet" type="text/css" href="styles/course_responsive.css">
    </head>
    <body>

        <div class="super_container">

            <!-- Header -->

            <%@include file="components/HeaderComponent.jsp" %>                         
            <!-- Home -->

            <div class="home">
                <div class="breadcrumbs_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="breadcrumbs">
                                    <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="courses.html">Courses</a></li>
                                        <li>Course Details</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>			
            </div>

            <!-- Course -->

            <div class="course">
                <div class="container">
                    <div class="col-md-8" style="float: left;">

                        <!-- Course -->
                        <div class="">

                            <div class="course_container">

                                <div class="course_title">${requestScope.listL.getLessonName()}</div>
<!--                                <div class="course_info d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">

                                     Course Info Item 
                                    <div class="course_info_item">
                                        <div class="course_info_title">Teacher:</div>
                                        <div class="course_info_text"><a href="#">Jacke Masito</a></div>
                                    </div>

                                     Course Info Item 
                                    <div class="course_info_item">
                                        <div class="course_info_title">Reviews:</div>
                                        <div class="rating_r rating_r_4"><i></i><i></i><i></i><i></i><i></i></div>
                                    </div>

                                     Course Info Item 
                                    <div class="course_info_item">
                                        <div class="course_info_title">Categories:</div>
                                        <div class="course_info_text"><a href="#">Languages</a></div>
                                    </div>

                                </div>-->

                                <!-- Course Image -->
                                <!--						<div class="course_image"><img src="images/course_image.jpg" alt=""></div>-->

                                <!-- Course Tabs -->
                                <div class="tab_panels" >
                                    <div class="course_tabs_container">
                                        <div class="tabs d-flex flex-row align-items-center justify-content-start">
                                            <div class="tab active">description</div>
                                            <div class="tab">curriculum</div>
                                            <div class="tab">reviews</div>
                                        </div>
                                        <div class ="tab_panels">
                                            <div class="row" style="display: inline;">
                                                <c:forEach items="${listD}" var="o">
                                                    <div class="tab_panels">
                                                        <div class="card">                                                                   
                                                            <div class="card-body">
                                                                <h1 class="tab_panel_title"> <p style="font-weight: bold;"title="View Leson">${o.getLessonContent()}</p></h1>                                                                       
                                                                <p class="tab_panel_text">${o.getLessonDescription()}</p>  
                                                                <img class="card-img-top" src="${o.getLessionImage()}" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>



                                    <!-- Reviews -->
                                    <div class="tab_panel tab_panel_3">
                                        <div class="tab_panel_title">Course Review</div>

                                        <!-- Rating -->
                                        <div class="review_rating_container">
                                            <div class="review_rating">
                                                <div class="review_rating_num">4.5</div>
                                                <div class="review_rating_stars">
                                                    <div class="rating_r rating_r_4"><i></i><i></i><i></i><i></i><i></i></div>
                                                </div>
                                                <div class="review_rating_text">(28 Ratings)</div>
                                            </div>
                                            <div class="review_rating_bars">
                                                <ul>
                                                    <li><span>5 Star</span><div class="review_rating_bar"><div style="width:90%;"></div></div></li>
                                                    <li><span>4 Star</span><div class="review_rating_bar"><div style="width:75%;"></div></div></li>
                                                    <li><span>3 Star</span><div class="review_rating_bar"><div style="width:32%;"></div></div></li>
                                                    <li><span>2 Star</span><div class="review_rating_bar"><div style="width:10%;"></div></div></li>
                                                    <li><span>1 Star</span><div class="review_rating_bar"><div style="width:3%;"></div></div></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <!-- Comments -->
                                        <div class="comments_container">
                                            <ul class="comments_list">
                                                <li>
                                                    <div class="comment_item d-flex flex-row align-items-start jutify-content-start">
                                                        <div class="comment_image"><div><img src="images/comment_1.jpg" alt=""></div></div>
                                                        <div class="comment_content">
                                                            <div class="comment_title_container d-flex flex-row align-items-center justify-content-start">
                                                                <div class="comment_author"><a href="#">Milley Cyrus</a></div>
                                                                <div class="comment_rating"><div class="rating_r rating_r_4"><i></i><i></i><i></i><i></i><i></i></div></div>
                                                                <div class="comment_time ml-auto">1 day ago</div>
                                                            </div>
                                                            <div class="comment_text">
                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have alteration in some form, by injected humour.</p>
                                                            </div>
                                                            <div class="comment_extras d-flex flex-row align-items-center justify-content-start">
                                                                <div class="comment_extra comment_likes"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i><span>15</span></a></div>
                                                                <div class="comment_extra comment_reply"><a href="#"><i class="fa fa-reply" aria-hidden="true"></i><span>Reply</span></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <div class="comment_item d-flex flex-row align-items-start jutify-content-start">
                                                                <div class="comment_image"><div><img src="images/comment_2.jpg" alt=""></div></div>
                                                                <div class="comment_content">
                                                                    <div class="comment_title_container d-flex flex-row align-items-center justify-content-start">
                                                                        <div class="comment_author"><a href="#">John Tyler</a></div>
                                                                        <div class="comment_rating"><div class="rating_r rating_r_4"><i></i><i></i><i></i><i></i><i></i></div></div>
                                                                        <div class="comment_time ml-auto">1 day ago</div>
                                                                    </div>
                                                                    <div class="comment_text">
                                                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have alteration in some form, by injected humour.</p>
                                                                    </div>
                                                                    <div class="comment_extras d-flex flex-row align-items-center justify-content-start">
                                                                        <div class="comment_extra comment_likes"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i><span>15</span></a></div>
                                                                        <div class="comment_extra comment_reply"><a href="#"><i class="fa fa-reply" aria-hidden="true"></i><span>Reply</span></a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div class="comment_item d-flex flex-row align-items-start jutify-content-start">
                                                        <div class="comment_image"><div><img src="images/comment_3.jpg" alt=""></div></div>
                                                        <div class="comment_content">
                                                            <div class="comment_title_container d-flex flex-row align-items-center justify-content-start">
                                                                <div class="comment_author"><a href="#">Milley Cyrus</a></div>
                                                                <div class="comment_rating"><div class="rating_r rating_r_4"><i></i><i></i><i></i><i></i><i></i></div></div>
                                                                <div class="comment_time ml-auto">1 day ago</div>
                                                            </div>
                                                            <div class="comment_text">
                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have alteration in some form, by injected humour.</p>
                                                            </div>
                                                            <div class="comment_extras d-flex flex-row align-items-center justify-content-start">
                                                                <div class="comment_extra comment_likes"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i><span>15</span></a></div>
                                                                <div class="comment_extra comment_reply"><a href="#"><i class="fa fa-reply" aria-hidden="true"></i><span>Reply</span></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="add_comment_container">
                                                <div class="add_comment_title">Add a review</div>
                                                <div class="add_comment_text">You must be <a href="#">logged</a> in to post a comment.</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Course Sidebar -->				
                    <div class="col-md-4" style="float: right;">
                        <div class= "sidebar">
                            <div class="sidebar_section">
                                <div class="card bg-light m-b-3">
                                    <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> List</div>
                                    <ul class="list-group category_block">
                                         <c:forEach items="${listE}" var="o">
                                            <li class="list-group-item text-white"><a href="LessonDetail?LessonID=${o.getLessonID()}">${o.getLessonName()}</a></li>
                                        </c:forEach>

                                    </ul>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Newsletter -->

<div class="newsletter">
    <div class="newsletter_background" style="background-image:url(images/newsletter_background.jpg)"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="newsletter_container d-flex flex-lg-row flex-column align-items-center justify-content-start">

                    <!-- Newsletter Content -->
                    <div class="newsletter_content text-lg-left text-center">
                        <div class="newsletter_title">sign up for news and offers</div>
                        <div class="newsletter_subtitle">Subcribe to lastest smartphones news & great deals we offer</div>
                    </div>

                    <!-- Newsletter Form -->
                    <div class="newsletter_form_container ml-lg-auto">
                        <form action="#" id="newsletter_form" class="newsletter_form d-flex flex-row align-items-center justify-content-center">
                            <input type="email" class="newsletter_input" placeholder="Your Email" required="required">
                            <button type="submit" class="newsletter_button">subscribe</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->

<footer class="footer">
    <div class="footer_background" style="background-image:url(images/footer_background.png)"></div>
    <div class="container">
        <div class="row footer_row">
            <div class="col">
                <div class="footer_content">
                    <div class="row">

                        <div class="col-lg-3 footer_col">

                            <!-- Footer About -->
                            <div class="footer_section footer_about">
                                <div class="footer_logo_container">
                                    <a href="#">
                                        <div class="footer_logo_text">Unic<span>at</span></div>
                                    </a>
                                </div>
                                <div class="footer_about_text">
                                    <p>Lorem ipsum dolor sit ametium, consectetur adipiscing elit.</p>
                                </div>
                                <div class="footer_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col">

                            <!-- Footer Contact -->
                            <div class="footer_section footer_contact">
                                <div class="footer_title">Contact Us</div>
                                <div class="footer_contact_info">
                                    <ul>
                                        <li>Email: Info.deercreative@gmail.com</li>
                                        <li>Phone:  +(88) 111 555 666</li>
                                        <li>40 Baria Sreet 133/2 New York City, United States</li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col">

                            <!-- Footer links -->
                            <div class="footer_section footer_links">
                                <div class="footer_title">Contact Us</div>
                                <div class="footer_links_container">
                                    <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                        <li><a href="#">Features</a></li>
                                        <li><a href="courses.html">Courses</a></li>
                                        <li><a href="#">Events</a></li>
                                        <li><a href="#">Gallery</a></li>
                                        <li><a href="#">FAQs</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col clearfix">

                            <!-- Footer links -->
                            <div class="footer_section footer_mobile">
                                <div class="footer_title">Mobile</div>
                                <div class="footer_mobile_content">
                                    <div class="footer_image"><a href="#"><img src="images/mobile_1.png" alt=""></a></div>
                                    <div class="footer_image"><a href="#"><img src="images/mobile_2.png" alt=""></a></div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row copyright_row">
            <div class="col">
                <div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
                    <div class="cr_text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
                    <div class="ml-lg-auto cr_links">
                        <ul class="cr_list">
                            <li><a href="#">Copyright notification</a></li>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/course.js"></script>
</body>
</html>