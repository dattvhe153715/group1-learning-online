/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Lesson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class LessonDao {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Lesson> getLessonList() {
        List<Lesson> t = new ArrayList<Lesson>();
        String query = "select * from Lesson";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            String xLessonName;
            int xLessonID, xCourseID;
            Lesson x;
            while (rs.next()) {
                xLessonID = rs.getInt("LessonID");
                xCourseID = rs.getInt("CourseID");
                xLessonName = rs.getString("LessonName");
                x = new Lesson(xLessonID, xCourseID, xLessonName);
                t.add(x);
            }
            conn.close();
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (t);
    }

    public List<Lesson> getLessonByCourse(int xCourseID) {
        List<Lesson> t = new ArrayList<Lesson>();
        Lesson x = null;
        String query = "select * from Lesson where CourseID = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, xCourseID);
            rs = ps.executeQuery();
            while (rs.next()) {
                t.add(new Lesson(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3)));

            }
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return t;
    }
    public Lesson getLessonbyLessonID(int xLessonID){
         Lesson x = null;
         String query = "select * from Lesson where LessonID = ?";
         try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, xLessonID);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Lesson(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3));
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         return null;
    }

}
