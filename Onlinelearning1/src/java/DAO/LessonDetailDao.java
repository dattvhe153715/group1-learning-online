package DAO;

import Model.LessonDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class LessonDetailDao {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<LessonDetail> getLessonDetailList() {
        List<LessonDetail> l = new ArrayList<LessonDetail>();
        String query = "select*from Lessondetail";
        try {
            conn =  new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            String xLessonContent, xLessionImage, xLessonDescription;
            int xLessonID, xLessonDetailID;
            LessonDetail x;
            while (rs.next()) {
                xLessonID = rs.getInt("LessonID");
                xLessonDetailID = rs.getInt("LessondetailID");
                xLessonContent = rs.getString("LessonContent");
                xLessionImage = rs.getString("LessionImage");

                xLessonDescription = rs.getString("LessonDescription");

                x = new LessonDetail(xLessonID, xLessonDetailID, xLessonContent, xLessionImage, xLessonDescription);
                l.add(x);
                
                
            }
            conn.close();
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return l;
    }

    public List<LessonDetail> getLessonDetailByLessonId(int xLessonID) {
        List<LessonDetail> t = new ArrayList<LessonDetail>();
        LessonDetail x = null;
        String query = "select * from Lessondetail where LessonID = ?";
        try {
            conn =  new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, xLessonID);
            rs = ps.executeQuery();
            String xLessonContent, xLessionImage, xLessonDescription;
            int xLessonDetailID;
            while (rs.next()) {
                xLessonID = rs.getInt("LessonID");
                xLessonDetailID = rs.getInt("LessondetailID");
                xLessonContent = rs.getString("LessonContent");
                xLessionImage = rs.getString("LessionImage");
                xLessonDescription = rs.getString("LessonDescription");
                x = new LessonDetail(xLessonID, xLessonDetailID, xLessonContent, xLessionImage, xLessonDescription);
                t.add(x);
            }
            conn.close();
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return t;
    }

    
    public static void main(String[] args) {
        LessonDetailDao u = new LessonDetailDao();
        List<LessonDetail> list = u.getLessonDetailList();
        

        for (LessonDetail o : list) {
            System.out.println(o);
        }
    }

}
