/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author tran dat
 */
public class User {
    private int userid;
    private String username;
    private String pass;
    private String phone;
    private String dob;
    private boolean isadmin;
    private boolean isastudent;

    public User() {
    }

    public User(int userid, String username, String pass, String phone, String dob, boolean isadmin, boolean isastudent) {
        this.userid = userid;
        this.username = username;
        this.pass = pass;
        this.phone = phone;
        this.dob = dob;
        this.isadmin = isadmin;
        this.isastudent = isastudent;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public boolean isIsadmin() {
        return isadmin;
    }

    public void setIsadmin(boolean isadmin) {
        this.isadmin = isadmin;
    }

    public boolean isIsastudent() {
        return isastudent;
    }

    public void setIsastudent(boolean isastudent) {
        this.isastudent = isastudent;
    }

    
}
