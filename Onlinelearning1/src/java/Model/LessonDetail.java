    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Van Minh
 */
public class LessonDetail {
    private int LesonID;
    private int LessonDetailID;
    private String LessonContent;
    private String LessionImage;
    private String LessonDescription;

    public LessonDetail() {
    }

    public LessonDetail(int LesonID, int LessonDetailID, String LessonContent, String LessionImage, String LessonDescription) {
        this.LesonID = LesonID;
        this.LessonDetailID = LessonDetailID;
        this.LessonContent = LessonContent;
        this.LessionImage = LessionImage;
        this.LessonDescription = LessonDescription;
    }

    public int getLesonID() {
        return LesonID;
    }

    public void setLesonID(int LesonID) {
        this.LesonID = LesonID;
    }

    public int getLessonDetailID() {
        return LessonDetailID;
    }

    public void setLessonDetailID(int LessonDetailID) {
        this.LessonDetailID = LessonDetailID;
    }

    public String getLessonContent() {
        return LessonContent;
    }

    public void setLessonContent(String LessonContent) {
        this.LessonContent = LessonContent;
    }

    public String getLessionImage() {
        return LessionImage;
    }

    public void setLessionImage(String LessionImage) {
        this.LessionImage = LessionImage;
    }

    public String getLessonDescription() {
        return LessonDescription;
    }

    public void setLessonDescription(String LessonDescription) {
        this.LessonDescription = LessonDescription;
    }

    
    @Override
    public String toString() {
        return "LessonDetail{" + "LesonID" + LesonID + ", LessondetailID=" + LessonDetailID + ", LessonContent=" + LessonContent + ", LessionImage=" + LessionImage + ", LessonDescription=" + LessonDescription + '}';
    }

   

   
    }

    