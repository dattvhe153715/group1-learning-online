/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author tran dat
 */
public class Question {
    private int qid;
    private String qtitle;

    public Question() {
    }

    public Question(int qid, String qtitle) {
        this.qid = qid;
        this.qtitle = qtitle;
    }

    public int getQid() {
        return qid;
    }

    public void setQid(int qid) {
        this.qid = qid;
    }

    public String getQtitle() {
        return qtitle;
    }

    public void setQtitle(String qtitle) {
        this.qtitle = qtitle;
    }
   
    
}
