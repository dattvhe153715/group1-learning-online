/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author tran dat
 */
public class Blog {
    private int blogID;
    private String blogtitle;
    private String blogdescription;

    public Blog() {
    }

    public Blog(int blogID, String blogtitle, String blogdescription) {
        this.blogID = blogID;
        this.blogtitle = blogtitle;
        this.blogdescription = blogdescription;
    }

    public int getBlogID() {
        return blogID;
    }

    public void setBlogID(int blogID) {
        this.blogID = blogID;
    }

    public String getBlogtitle() {
        return blogtitle;
    }

    public void setBlogtitle(String blogtitle) {
        this.blogtitle = blogtitle;
    }

    public String getBlogdescription() {
        return blogdescription;
    }

    public void setBlogdescription(String blogdescription) {
        this.blogdescription = blogdescription;
    }
    
    
}
