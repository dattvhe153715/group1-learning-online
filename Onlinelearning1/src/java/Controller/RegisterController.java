/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RegisterDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Model.User;
import java.util.ArrayList;

/**
 *
 * @author tran dat
 */
public class RegisterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegisterController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegisterController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String pass = request.getParameter("pass");
        String phone = request.getParameter("phone");

        String phonee = "84" + phone;
        String repass = request.getParameter("repass");
        String dob = request.getParameter("dob");
        String admin = "0";
        String student = "1";
        RegisterDao ls = new RegisterDao();
        ArrayList<User> u = ls.verifyregister(username, phonee);
        int a = 0;
        if (dob.trim().equals("")) {
            request.setAttribute("dobs", "dob not null...");
            a++;
        }
        for(User user : u) {
            if (user.getUsername().equalsIgnoreCase(username) ) {
                request.setAttribute("usernames", "username exist...");
                a++;
            }
            if (user.getPhone().equals(phonee) ) {
                request.setAttribute("phones", "phone exist ...");
                a++;
            }
        }
        if (username.trim().equals("") || username == null) {
            request.setAttribute("user", "uername not null...");
            a++;
        }
        if (phone.trim().equals("") || phone == null) {
            request.setAttribute("phonee", "phone not null...");
            a++;
        }
        if (pass.trim().equals("") || pass == null) {
            request.setAttribute("passs", "pass not null...");
            a++;
        }
        if (repass.trim().equals("") || repass == null) {
            request.setAttribute("repasss", "repass not null...");
            a++;
        }
        if(pass.equals(repass)) {
        } else {
            request.setAttribute("notice", "wrong repass...");
            a++;
        }
        if(a == 0) {
            ls.Register(username, pass, phone, dob, admin, student);
            request.getRequestDispatcher("login").forward(request, response);
        }else{
        request.getRequestDispatcher("register.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
