/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RegisterDao;
import DAO.userProfileDao;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tran dat
 */
public class editprofileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet editprofileController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet editprofileController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userid = session.getAttribute("iduser").toString();
        
        String name =  request.getParameter("name");
        String phone = request.getParameter("phone");
        String dob = request.getParameter("dob");
        String pass = request.getParameter("pass");
        String repass = request.getParameter("repass");
        
        userProfileDao ufp = new userProfileDao();
        int a = 0;
        if(name.trim().equals("") || name==null){
            
        }else{
            User u = ufp.checkName( name);
            if(u!=null){
                request.setAttribute("namess", "Username exist..!");
                a++;
            }else{
            ufp.updateUserName(userid,name);
            }
        }
        if(phone.trim().equals("") || phone==null){
            
        }else{
            User up = ufp.checkPhone( phone);
            if(up!=null){
                request.setAttribute("phoneee", "Phone exist..!");
                a++;
            }else{
            ufp.updatePhone(userid,phone);
            }
        }
        if(dob.trim().equals("") || dob==null){
            
        }else{
            ufp.updateDOB(userid,dob);
        }
        if(pass.trim().equals("") || pass==null){
            
        }else{
            ufp.updatePassWord(userid,pass);
        }
        if(pass.equals(repass)){
        }else{
            a++;
            request.setAttribute("notice", "wrong rePassWord");
        }
        if(a==0){
            request.setAttribute("notices", "update successful..");
            request.getRequestDispatcher("userprofile").forward(request, response);
        }else{
            request.getRequestDispatcher("userprofile").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
