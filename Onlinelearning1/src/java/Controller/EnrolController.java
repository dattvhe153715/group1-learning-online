/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Course;
import DAO.CourseDao;
import DAO.LessonDao;
import Model.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "EnrolController", urlPatterns = {"/Enrrol"})
public class EnrolController extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String xCourseID = request.getParameter("CourseID").trim();
        int CourseID = Integer.parseInt(xCourseID);
        CourseDao dao = new CourseDao();
        Course listCourse = dao.getCoursebyCourseID(CourseID);
        LessonDao l = new LessonDao();
        List<Lesson> listLesson = l.getLessonByCourse(CourseID);
        request.setAttribute("list", listCourse);
        request.setAttribute("listE", listLesson);
        request.getRequestDispatcher("enrrol.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}