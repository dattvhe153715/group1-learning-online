/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.userProfileDao;
import DAO.userProfileDao;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Model.Course;
import java.util.List;
import DAO.RegisterDao;
/**
 *
 * @author tran dat
 */
public class userprofileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        
        String userid = session.getAttribute("iduser").toString();
       

        userProfileDao uf = new userProfileDao();
        User u = uf.Uprofile(userid);
        List<Course> listcn = uf.getCourseNameEnroll(userid);
        request.setAttribute("listcn", listcn);
        String name = u.getUsername();
        String phone = u.getPhone(); 
        String dob = u.getDob();
        request.setAttribute("name", name);
        request.setAttribute("dob", dob);
        request.setAttribute("phone", phone);

        request.getRequestDispatcher("userprofile.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
//        HttpSession session = request.getSession();
//        
//        String userid = session.getAttribute("iduser").toString();
//       
//
//        userProfileDao uf = new userProfileDao();
//        User u = uf.Uprofile(userid);
//        List<Course> listcn = uf.getCourseNameEnroll(userid);
//        request.setAttribute("listcn", listcn);
//        String name = u.getUsername();
//        String phone = u.getPhone(); 
//        String dob = u.getDob();
//        request.setAttribute("name", name);
//        request.setAttribute("dob", dob);
//        request.setAttribute("phone", phone);
//
//        request.getRequestDispatcher("userprofile.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
