<%-- 
    Document   : addblog
    Created on : Jul 20, 2022, 3:31:51 PM
    Author     : tran dat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Addblog</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
        <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
        <link rel="stylesheet" type="text/css" href="styles/courses.css">
        <link rel="stylesheet" type="text/css" href="styles/courses_responsive.css">
</head>
<body>
    <%@include file="components/HeaderComponent.jsp" %>
    <div class="home">
                <div class="breadcrumbs_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="breadcrumbs">
                                    <ul>
                                        <li><a href="courselist">Home</a></li>
                                        <li><a href="bloglist">Blog</a></li>
                                        <li>Create blog</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>			
            </div>
    
    <div style="padding-top: 50px;padding-bottom: 30px">
    <form method="post" action="addblog" enctype="multipart/form-data" style="text-align: center">
     <div>
      <label>Profile Photo: </label> 
      <input type="file" name="photo" size="100" />
     </div>
        <br>
       <div>
      <label>Name blog: </label> 
      <input type="text" name="blogtitle" placeholder=" enter blogtitle....!"size="70"style="height: 40px" />
     </div>
        <br>
      <div >
          <p style="margin-right: 600px">Description :</p>
      <textarea name="blogdescription" rows="7" cols="90" placeholder=" enter detail about blog .....!" ></textarea>
      </div>
        
        <input type="submit" value="Created blog" style="margin-left: 300px;padding: 5px 20px 5px 20px;background-color: #48D1CC">
        
    </form>
   </div>
   <%@include file="components/FooterComponent.jsp" %>
</body>
</html>
