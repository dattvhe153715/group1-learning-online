<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--  This file has been downloaded from bootdey.com @bootdey on twitter -->
        <!--  All snippets are MIT license http://bootdey.com/license -->
        <title>profile </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
        <meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
        <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
        <link rel="stylesheet" type="text/css" href="styles/courses.css">
        <link rel="stylesheet" type="text/css" href="styles/courses_responsive.css">
    </head>
    <body>
        <%@include file="components/HeaderComponent.jsp" %>
        <div class="home">
                <div class="breadcrumbs_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="breadcrumbs">
                                    <ul>
                                        <li><a href="courselist">Home</a></li>
                                        <li>User profile</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>			
            </div>
        <div class="container" style="padding-top: 30px">
            <div class="main-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-column align-items-center text-center">
                                    <img src="https://www.pjkconsulting.co.za/assets/images/team/team-3.jpg" alt="Admin" class="rounded-circle p-1 bg-primary" width="110">
                                    <div class="mt-3">
                                        <h4>${name}</h4>
                                        <h6>${dob}</h6>

                                    </div>
                                </div>
                                <hr class="my-4">
                                <div> My Course</div>
                                <c:forEach items = "${listcn}" var = "lc">
                                    <hr class="my-4">
                                    <div>
                                        <li>${lc.getCourseName()} </li><a href="unenrolled?courseID=${lc.getCourseID()}">Quit</a>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="card">

                            <div class="card-body">
                                <form action="editprofile" method="post">
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Username</h6>
                                            <span style="color:red;">${namess}</span>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" name="name" class="form-control" placeholder="${name}">
                                        </div>

                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Date of birth</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="date" name="dob" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Mobile Phone</h6>
                                            <span style="color:red;">${phoneee}</span>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" name="phone" class="form-control" placeholder="${phone}">
                                        </div>

                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">New password</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="password" name="pass" class="form-control" placeholder="*******">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Retype password</h6>
                                            <span style="color:red;">${notice}</span>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="password" name="repass" class="form-control" placeholder="*******">
                                        </div>

                                    </div>
                                    <span style="color:green;">${notices}</span>
                                    <div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                        </div>
                                    </div>
                                </form>
                                
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <style type="text/css">
            body{
                background: #f7f7ff;
                margin-top:20px;
            }
            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid transparent;
                border-radius: .25rem;
                margin-bottom: 1.5rem;
                box-shadow: 0 2px 6px 0 rgb(218 218 253 / 65%), 0 2px 6px 0 rgb(206 206 238 / 54%);
            }
            .me-2 {
                margin-right: .5rem!important;
            }
        </style>

        <script type="text/javascript">

        </script>
         <%@include file="components/FooterComponent.jsp" %>
    </body>
</html>