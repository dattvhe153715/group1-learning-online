<%-- 
    Document   : blogdetail
    Created on : Jun 28, 2022, 3:07:44 PM
    Author     : tran dat
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Blog Single</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Unicat project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
        <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="styles/blog_single.css">
        <link rel="stylesheet" type="text/css" href="styles/blog_single_responsive.css">
        <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
        <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
        <link rel="stylesheet" type="text/css" href="styles/courses.css">
        <link rel="stylesheet" type="text/css" href="styles/courses_responsive.css">

        <style>
            .dropbtn {

                color: black;
                padding: 16px;
                font-size: 16px;
                border: none;
                cursor: pointer;
            }

            .dropbtn:hover, .dropbtn:focus {
                background-color: #fb6868;
            }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #59e9cc;
                min-width: 160px;
                overflow: auto;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }
            .show {display:block;}
        </style>


        <script type="text/javascript">

            function testAlertDialog() {

                var result = confirm("if you want to comment you need to login \n login now...?");

                if (result) {
                    window.location.href = "login.jsp";
                } else {


                }
            }

        </script>
    </head>
    <body>
        <script type="text/javascript">
            // Get the button, and when the user clicks on it, execute myFunction
            document.getElementById("myBtn").onclick = function () {
                myFunction();
            };
            document.getElementById("123").onclick = function () {
                myFunction();
            };

            /* myFunction toggles between adding and removing the show class, which is used to hide and show the dropdown content */
            function myFunction() {
                document.getElementById("myDropdown").classList.toggle("show");
            }
        </script>

        <div class="super_container">

            <!-- Header -->

            <%@include file="components/HeaderComponent.jsp" %>

            <!-- Home -->

            <div class="home">
                <div class="breadcrumbs_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="breadcrumbs">
                                    <ul>
                                        <li><a href="courselist">Home</a></li>
                                        <li><a href="bloglist">Blog</a></li>
                                        <li>Blog Single</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>			
            </div>

            <!-- Blog -->

            <div class="blog">
                <div class="container">
                    <div class="row">

                        <!-- Blog Content -->
                        <div class="col-lg-8">

                            <div class="blog_content">
                                <div class="blog_title">${blogtitle}</div>
                                <div class="blog_meta">
                                    <ul>
                                        <li>Post on <a href="#"><fmt:formatDate type="date" value="${bdate}" /></a></li>
                                        <li>By <a href="#">${username}</a></li>


                                    </ul>
                                </div>

                                <div class="blog_image" style="padding-left: 120px"><img src="${blogImage}" alt=""></div>

                                <div class="blog_quote d-flex flex-row align-items-center justify-content-start">
                                    <i class="fa fa-quote-left" aria-hidden="true"></i>
                                    <div>${blogdescription}</div>
                                </div>
                                <p>But.</p>
                            </div>

                            <div class="blog_extra d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                                <div class="blog_tags">
                                    <span>Tags: </span>
                                    <ul>
                                        <li><a href="#">Education</a>, </li>
                                        <li><a href="#">Math</a>, </li>
                                        <li><a href="#">Food</a>, </li>
                                        <li><a href="#">Schools</a>, </li>
                                        <li><a href="#">Religion</a>, </li>
                                    </ul>
                                </div>
                                <div class="blog_social ml-lg-auto">
                                    <span>Share: </span>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Comments -->
                            <div class="comments_container">
                                <div class="comments_title"><span></span> Comments</div>
                                <ul class="comments_list">
                                    <li>

                                        <div style="padding-left: 110px;" >



                                            <c:choose>
                                                <c:when test="${iduserss == 'null'}">

                                                    <input type="hidden" name="blogID" value="${blogID}">
                                                    <input type="text" name="bfbtitle" placeholder=" enter comment here..!" style="width: 550px;height: 40px;border-radius: 10px">
                                                    <input type="submit"  name="posts" value="Post" onclick="testAlertDialog();" style="background-color: #48D1CC;width: 70px;height: 40px;border-radius: 10px">

                                                </c:when>    
                                                <c:otherwise>
                                                    <form action="addnewblogfeedback" method="POST">
                                                        <input type="hidden" name="blogID" value="${blogID}">
                                                        <input type="text" name="bfbtitle" placeholder=" enter comment here..!" style="width: 550px;height: 40px;border-radius: 10px">
                                                        <input type="submit" name="post" value="Post" style="background-color: #48D1CC;width: 70px;height: 40px;border-radius: 10px">
                                                    </form>
                                                </c:otherwise>
                                            </c:choose> 





                                        </div>

                                        <c:forEach items="${requestScope.BlogFeedback}" var="bfb">
                                            <div class="comment_item d-flex flex-row align-items-start jutify-content-start">
                                                <div class="comment_image"><div><img src="images/comment_1.jpg" alt=""></div></div>
                                                <div class="comment_content">

                                                    <div class="comment_title_container d-flex flex-row align-items-center justify-content-start">
                                                        <div class="comment_author"><a href="#">${bfb.user.username}</a></div>
                                                        <div class="comment_rating"><div class="rating_r rating_r_4"><i></i><i></i><i></i><i></i><i></i></div></div>
                                                        <div class="comment_time ml-auto"><fmt:formatDate type="date" value="${bfb.date}" /></div>
                                                    </div>
                                                    <div class="comment_text">
                                                        <p>${bfb.bfbtitle}</p>
                                                    </div>
                                                    <div class="comment_extras d-flex flex-row align-items-center justify-content-start">

                                                    </div>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <c:choose>
                                                        <c:when test="${iduserss == 'null'}">

                                                            <input type="hidden" name="bfbid" value="${bfb.bfbid}">
                                                            <input type="text" name="comment" placeholder="    answer here..!" style="width: 460px;height: 30px;border-radius: 10px">
                                                            <input type="submit" value="Post" onclick="testAlertDialog();" style="background-color: #48D1CC;width: 60px;height: 30px;border-radius: 10px">

                                                        </c:when>    
                                                        <c:otherwise>
                                                            <div style="padding-left: 110px; " >
                                                                <form action="addnewcomment" method="post">
                                                                    <input type="hidden" name="bfbid" value="${bfb.bfbid}">
                                                                    <input type="hidden" name="blogid" value="${blogID}">
                                                                    <input type="text" name="comment" placeholder="    answer here..!" style="width: 460px;height: 30px;border-radius: 10px">
                                                                    <input type="submit" value="Post" style="background-color: #48D1CC;width: 60px;height: 30px;border-radius: 10px">
                                                                </form>
                                                            </div>
                                                        </c:otherwise>
                                                    </c:choose> 

                                                    <c:forEach items="${bfb.comment}" var="c">
                                                        <div class="comment_item d-flex flex-row align-items-start jutify-content-start">
                                                            <div class="comment_image"><div><img src="images/comment_2.jpg" alt=""></div></div>
                                                            <div class="comment_content">
                                                                <div class="comment_title_container d-flex flex-row align-items-center justify-content-start">
                                                                    <div class="comment_author"><a href="#">${c.user.username}</a></div>
                                                                    <div class="comment_rating"><div class="rating_r rating_r_4"><i></i><i></i><i></i><i></i><i></i></div></div>
                                                                    <div class="comment_time ml-auto"><fmt:formatDate type="date" value="${c.cdate}" /></div>
                                                                </div>
                                                                <div class="comment_text">
                                                                    <p>${c.cmttitle}</p>
                                                                </div>
                                                                <div class="comment_extras d-flex flex-row align-items-center justify-content-start">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </li>
                                            </ul>
                                        </c:forEach>
                                    </li>

                                </ul>
                            </div>
                        </div>

                        <!-- Blog Sidebar -->
                        <div class="col-lg-4">
                            <div class="sidebar">

                                <!-- Latest News -->
                                <!--						<div class="sidebar_section">
                                                                                        <div class="sidebar_section_title">Latest Courses</div>
                                                                                        <div class="sidebar_latest">
                                
                                                                                                 Latest Course 
                                                                                                <div class="latest d-flex flex-row align-items-start justify-content-start">
                                                                                                        <div class="latest_image"><div><img src="images/latest_1.jpg" alt=""></div></div>
                                                                                                        <div class="latest_content">
                                                                                                                <div class="latest_title"><a href="course.html">How to Design a Logo a Beginners Course</a></div>
                                                                                                                <div class="latest_date">november 11, 2017</div>
                                                                                                        </div>
                                                                                                </div>
                                
                                                                                                 Latest Course 
                                                                                                <div class="latest d-flex flex-row align-items-start justify-content-start">
                                                                                                        <div class="latest_image"><div><img src="images/latest_2.jpg" alt=""></div></div>
                                                                                                        <div class="latest_content">
                                                                                                                <div class="latest_title"><a href="course.html">Photography for Beginners Masterclass</a></div>
                                                                                                                <div class="latest_date">november 11, 2017</div>
                                                                                                        </div>
                                                                                                </div>
                                
                                                                                                 Latest Course 
                                                                                                <div class="latest d-flex flex-row align-items-start justify-content-start">
                                                                                                        <div class="latest_image"><div><img src="images/latest_3.jpg" alt=""></div></div>
                                                                                                        <div class="latest_content">
                                                                                                                <div class="latest_title"><a href="course.html">The Secrets of Body Language</a></div>
                                                                                                                <div class="latest_date">november 11, 2017</div>
                                                                                                        </div>
                                                                                                </div>
                                
                                                                                        </div>
                                                                                </div>-->



                                <!-- Tags -->


                                <!-- Banner -->
                                <div class="sidebar_section">
                                    <div class="sidebar_banner d-flex flex-column align-items-center justify-content-center text-center">
                                        <div class="sidebar_banner_background" style="background-image:url(images/banner_1.jpg)"></div>
                                        <div class="sidebar_banner_overlay"></div>
                                        <div class="sidebar_banner_content">
                                            <div class="banner_title">Free Book</div>
                                            <div class="banner_button"><a href="#">download now</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sidebar_section">
                                    <c:choose>
                                        <c:when test="${iduserss == userid}">

                                            <div class="dropdown"  >
                                                <div >
                                                    <div> <span style="color: red">${delete}</span></div>
                                                    <button id="myBtn" class="dropbtn" style="margin-left: 80px;width: 150px;height: 50px" onclick="myFunction();">Delete</button>
                                                </div>

                                                <div id="myDropdown" class="dropdown-content" style="height: 130px;width: 300px;background-color: #f2f6f5;float: right">


                                                    <a>enter <span style="color: red">" delete "</span> in the box below if you want to delete this blog..!</a>
                                                    <form action="deleteblog" method="post">
                                                        <input type="hidden" name="blogid" value="${blogID}">         
                                                        <input type="text" name="delete" >
                                                        <input type="submit"  value="delete" style="width: 80px; background-color: #fb6868"/>
                                                    </form>
                                                    <button id="123" onclick="myFunction();" style="width: 170px;margin-top: 5px">cancer</button>

                                                </div>

                                            </div>

                                        </c:when>    
                                        <c:otherwise>
                                        </c:otherwise>
                                    </c:choose> 
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Newsletter -->

            <div class="newsletter">
                <div class="newsletter_background" style="background-image:url(images/newsletter_background.jpg)"></div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="newsletter_container d-flex flex-lg-row flex-column align-items-center justify-content-start">

                                <!-- Newsletter Content -->
                                <div class="newsletter_content text-lg-left text-center">
                                    <div class="newsletter_title">sign up for news and offers</div>
                                    <div class="newsletter_subtitle">Subcribe to lastest smartphones news & great deals we offer</div>
                                </div>

                                <!-- Newsletter Form -->
                                <div class="newsletter_form_container ml-lg-auto">
                                    <form action="#" id="newsletter_form" class="newsletter_form d-flex flex-row align-items-center justify-content-center">
                                        <input type="email" class="newsletter_input" placeholder="Your Email" required="required">
                                        <button type="submit" class="newsletter_button">subscribe</button>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->

            <footer class="footer">
                <div class="footer_background" style="background-image:url(images/footer_background.png)"></div>
                <div class="container">
                    <div class="row footer_row">
                        <div class="col">
                            <div class="footer_content">
                                <div class="row">

                                    <div class="col-lg-3 footer_col">

                                        <!-- Footer About -->
                                        <div class="footer_section footer_about">
                                            <div class="footer_logo_container">
                                                <a href="#">
                                                    <div class="footer_logo_text">Unic<span>at</span></div>
                                                </a>
                                            </div>
                                            <div class="footer_about_text">
                                                <p>Lorem ipsum dolor sit ametium, consectetur adipiscing elit.</p>
                                            </div>
                                            <div class="footer_social">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-3 footer_col">

                                        <!-- Footer Contact -->
                                        <div class="footer_section footer_contact">
                                            <div class="footer_title">Contact Us</div>
                                            <div class="footer_contact_info">
                                                <ul>
                                                    <li>Email: Info.deercreative@gmail.com</li>
                                                    <li>Phone:  +(88) 111 555 666</li>
                                                    <li>40 Baria Sreet 133/2 New York City, United States</li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-3 footer_col">

                                        <!-- Footer links -->
                                        <div class="footer_section footer_links">
                                            <div class="footer_title">Contact Us</div>
                                            <div class="footer_links_container">
                                                <ul>
                                                    <li><a href="index.html">Home</a></li>
                                                    <li><a href="about.html">About</a></li>
                                                    <li><a href="contact.html">Contact</a></li>
                                                    <li><a href="#">Features</a></li>
                                                    <li><a href="courses.html">Courses</a></li>
                                                    <li><a href="#">Events</a></li>
                                                    <li><a href="#">Gallery</a></li>
                                                    <li><a href="#">FAQs</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-3 footer_col clearfix">

                                        <!-- Footer links -->
                                        <div class="footer_section footer_mobile">
                                            <div class="footer_title">Mobile</div>
                                            <div class="footer_mobile_content">
                                                <div class="footer_image"><a href="#"><img src="images/mobile_1.png" alt=""></a></div>
                                                <div class="footer_image"><a href="#"><img src="images/mobile_2.png" alt=""></a></div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row copyright_row">
                        <div class="col">
                            <div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
                                <div class="cr_text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
                                <div class="ml-lg-auto cr_links">
                                    <ul class="cr_list">
                                        <li><a href="#">Copyright notification</a></li>
                                        <li><a href="#">Terms of Use</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="styles/bootstrap4/popper.js"></script>
        <script src="styles/bootstrap4/bootstrap.min.js"></script>
        <script src="plugins/easing/easing.js"></script>
        <script src="plugins/parallax-js-master/parallax.min.js"></script>
        <script src="plugins/colorbox/jquery.colorbox-min.js"></script>
        <script src="js/blog_single.js"></script>
    </body>
</html>
