/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author tran dat
 */
public class Blog {
    private int blogID;
    private int uID;
    private String blogtitle;
    private String blogImage;
    private String blogdescription;
    private Date bdate;
    private User user;

    public Blog() {
    }

    public Blog(int blogID, int uID, String blogtitle, String blogImage, String blogdescription, Date bdate, User user) {
        this.blogID = blogID;
        this.uID = uID;
        this.blogtitle = blogtitle;
        this.blogImage = blogImage;
        this.blogdescription = blogdescription;
        this.bdate = bdate;
        this.user = user;
    }

    public int getBlogID() {
        return blogID;
    }

    public void setBlogID(int blogID) {
        this.blogID = blogID;
    }

    public int getuID() {
        return uID;
    }

    public void setuID(int uID) {
        this.uID = uID;
    }

    public String getBlogtitle() {
        return blogtitle;
    }

    public void setBlogtitle(String blogtitle) {
        this.blogtitle = blogtitle;
    }

    public String getBlogImage() {
        return blogImage;
    }

    public void setBlogImage(String blogImage) {
        this.blogImage = blogImage;
    }

    public String getBlogdescription() {
        return blogdescription;
    }

    public void setBlogdescription(String blogdescription) {
        this.blogdescription = blogdescription;
    }

    public Date getBdate() {
        return bdate;
    }

    public void setBdate(Date bdate) {
        this.bdate = bdate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    

    

   
    
    
}
