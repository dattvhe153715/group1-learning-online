/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author tran dat
 */
public class Comment {
    private int cmtid;
    private int bfbid;
    private Date cdate;
    private String cmttitle;
    private User user;
    private BlogFeedback blogfeedback ;

    public int getCmtid() {
        return cmtid;
    }

    public void setCmtid(int cmtid) {
        this.cmtid = cmtid;
    }

    public int getBfbid() {
        return bfbid;
    }

    public void setBfbid(int bfbid) {
        this.bfbid = bfbid;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }

    public String getCmttitle() {
        return cmttitle;
    }

    public void setCmttitle(String cmttitle) {
        this.cmttitle = cmttitle;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BlogFeedback getBlogfeedback() {
        return blogfeedback;
    }

    public void setBlogfeedback(BlogFeedback blogfeedback) {
        this.blogfeedback = blogfeedback;
    }

    
    

   

   
    
}
