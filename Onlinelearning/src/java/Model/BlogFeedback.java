/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author tran dat
 */
public class BlogFeedback {
    private int bfbid;
    private int blogID;
    private int uid;
    private String bfbtitle;
    private Date date;
    private User user;
    private ArrayList<Comment> comment = new ArrayList<>();

    public int getBfbid() {
        return bfbid;
    }

    public void setBfbid(int bfbid) {
        this.bfbid = bfbid;
    }

    public int getBlogID() {
        return blogID;
    }

    public void setBlogID(int blogID) {
        this.blogID = blogID;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getBfbtitle() {
        return bfbtitle;
    }

    public void setBfbtitle(String bfbtitle) {
        this.bfbtitle = bfbtitle;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Comment> getComment() {
        return comment;
    }

    public void setComment(ArrayList<Comment> comment) {
        this.comment = comment;
    }

    

   
    
}
