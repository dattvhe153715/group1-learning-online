/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Van Minh
 */
public class Course {
    private int CourseID, categoryID;
    private String CourseName;
    private String CourseImage;
    private int CouseCreator;
    private String CourseInf,CourseDiscription;

    public Course() {
    }

    public Course(int CourseID, int categoryID, String CourseName, String CourseImage, int CouseCreator, String CourseInf, String CourseDiscription) {
        this.CourseID = CourseID;
        this.categoryID = categoryID;
        this.CourseName = CourseName;
        this.CourseImage = CourseImage;
        this.CouseCreator = CouseCreator;
        this.CourseInf = CourseInf;
        this.CourseDiscription = CourseDiscription;
    }

    public int getCourseID() {
        return CourseID;
    }

    public void setCourseID(int CourseID) {
        this.CourseID = CourseID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String CourseName) {
        this.CourseName = CourseName;
    }

    public String getCourseImage() {
        return CourseImage;
    }

    public void setCourseImage(String CourseImage) {
        this.CourseImage = CourseImage;
    }

    public int getCouseCreator() {
        return CouseCreator;
    }

    public void setCouseCreator(int CouseCreator) {
        this.CouseCreator = CouseCreator;
    }

    public String getCourseInf() {
        return CourseInf;
    }

    public void setCourseInf(String CourseInf) {
        this.CourseInf = CourseInf;
    }

    public String getCourseDiscription() {
        return CourseDiscription;
    }

    public void setCourseDiscription(String CourseDiscription) {
        this.CourseDiscription = CourseDiscription;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    
}

