/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Model.Course;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author tran dat
 */
public class userProfileDao {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public User Uprofile(String uid) {
        try {
            String sql = "SELECT * \n"
                    + "  FROM [dbo].[User]\n"
                    + "  where UserID = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, uid);
            rs = ps.executeQuery();
            if (rs.next()) {
                User u = new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getBoolean(7));
                return u;
            }
        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                ps.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                conn.close();
            } catch (SQLException e) {
                /* ignored */ }
        }
        return null;
    }

    public List<Course> getCourseNameEnroll(String uid) {
        List<Course> cn = new ArrayList<>();

        try {
            String sql = "select c.CourseID,c.CategoryID,c.CourseName,c.CourseImage,c.CourseCreator,c.CourseInf,c.CourseDescription\n"
                    + "from [User] u inner join [Enroll] e on u.UserID = e.UserID\n"
                    + "inner join Course  c on c.CourseID = e.CourseID\n"
                    + "where u.UserID = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, uid);
            rs = ps.executeQuery();
            while (rs.next()) {
                cn.add(new Course(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7)));
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cn;
    }

    public void deleteUnenrolledCourse(String iduser, String courseid) {
            try {
                String sql = "DELETE FROM [dbo].[Enroll]\n"
                + "      WHERE UserID = ? and CourseID = ?";
                conn = new DBContext().getConnection();
                ps = conn.prepareStatement(sql);
                ps.setString(1, iduser);
                ps.setString(2, courseid);
                ps.executeUpdate();
                
                ps.close();
                conn.close();
            } catch (Exception ex) {
                Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        
        
    }

    public User checkName(String name) {
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[User]\n"
                    + "  where UserName = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                User u = new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getBoolean(7));
                return u;
            }

        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public User checkPhone(String phone) {
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[User]\n"
                    + "  where Phone = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, phone);
            rs = ps.executeQuery();
            if (rs.next()) {
                User u = new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getBoolean(7));
                return u;
            }

        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateUserName(String iduser, String name) {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET [UserName] = ?\n"
                    + "   \n"
                    + " WHERE [UserID] = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, iduser);
            ps.executeUpdate();
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updatePhone(String iduser, String phone) {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET [Phone] = ?\n"
                    + "   \n"
                    + " WHERE [UserID] = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, phone);
            ps.setString(2, iduser);
            ps.executeUpdate();
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateDOB(String iduser, String dob) {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET [Phone] = ?\n"
                    + "   \n"
                    + " WHERE [UserID] = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dob);
            ps.setString(2, iduser);
            ps.executeUpdate();
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updatePassWord(String iduser, String pass) {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET [PassWord] = ?\n"
                    + "   \n"
                    + " WHERE [UserID] = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, pass);
            ps.setString(2, iduser);
            ps.executeUpdate();
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(userProfileDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) {
        LocalDate dat = java.time.LocalDate.now();
        System.out.println(dat);
    }
}
