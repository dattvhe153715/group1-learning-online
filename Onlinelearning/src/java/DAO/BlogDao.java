/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import Model.Blog;
import Model.User;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tran dat
 */
public class BlogDao {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<Blog> listBlog() {
        ArrayList<Blog> blog = new ArrayList<>();
        try {
            String sql = "SELECT  b.BlogID,b.UserID,b.BlogTitler,b.BlogImage,b.BlogDescription,b.bdate,u.UserName,u.[Admin]\n"
                    + "                      FROM [Blogg] b inner join [User] u\n"
                    + "                     on b.UserID = u.UserID\n"
                    + "					 order by b.BlogID desc";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUsername(rs.getString(7));
                u.setIsadmin(rs.getBoolean(8));
                Blog b = new Blog(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        u);
                blog.add(b);

            }

        } catch (Exception ex) {
            Logger.getLogger(BlogDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
            }
            try {
                ps.close();
            } catch (SQLException e) {
            }
            try {
                conn.close();
            } catch (SQLException e) {
            }
        }
        return blog;
    }

    public Blog blogSingleByBlogID(int blogID) {
        try {
            String sql = "SELECT  b.BlogID,b.UserID,b.BlogTitler,b.BlogImage,b.BlogDescription,b.bdate,u.UserName,u.[Admin],u.UserID\n"
                    + "                    FROM [Blogg] b inner join [User] u\n"
                    + "                     on b.UserID = u.UserID\n"
                    + "                    where b.BlogID = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, blogID);
            rs = ps.executeQuery();
            if (rs.next()) {
                User u = new User();
                u.setUsername(rs.getString(7));
                u.setIsadmin(rs.getBoolean(8));
                u.setUserid(rs.getInt(9));
                Blog blog = new Blog(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        u);
                return blog;

            }

        } catch (Exception ex) {
            Logger.getLogger(BlogDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
            }
            try {
                ps.close();
            } catch (SQLException e) {
            }
            try {
                conn.close();
            } catch (SQLException e) {
            }
        }
        return null;
    }

    public void insertBlog(Blog blog, String bdate) {

        try {
            String sql = "INSERT INTO [dbo].[Blogg]\n"
                    + "           ([UserID]\n"
                    + "           ,[BlogTitler]\n"
                    + "           ,[BlogImage]\n"
                    + "           ,[BlogDescription]\n"
                    + "           ,[bdate])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, blog.getuID());
            ps.setString(2, blog.getBlogtitle());
            ps.setString(3, blog.getBlogImage());
            ps.setString(4, blog.getBlogdescription());
            ps.setString(5, bdate);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(BlogDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                ps.close();
            } catch (SQLException e) {
            }
            try {
                conn.close();
            } catch (SQLException e) {
            }

        }

    }

    public void deleteBlog(String blogID) {

        try {
            String sql = "begin try \n"
                    + "	begin tran\n"
                    + "	declare @blogid_delete int = ?\n"
                    + "\n"
                    + "	delete from comments where BlogID= @blogid_delete\n"
                    + "	delete from BlogFeedback where BlogID= @blogid_delete\n"
                    + "	delete from Blogg where BlogID= @blogid_delete\n"
                    + "	commit tran\n"
                    + "end try\n"
                    + "begin catch\n"
                    + " rollback tran\n"
                    + "end catch";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, blogID);
            ps.executeUpdate();

            ps.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(BlogDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
