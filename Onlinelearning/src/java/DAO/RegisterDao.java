/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tran dat
 */
public class RegisterDao {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList verifyregister(String username, String phone) {
        ArrayList<User> listu = new ArrayList<>();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[User]\n"
                    + "  where [UserName] = ? or Phone = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, phone);
//            đưa cái cần where vào câu lệnh trên kia
            rs = ps.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getBoolean(7));
                listu.add(u);
                return listu;

            }
        } catch (Exception ex) {
            Logger.getLogger(RegisterDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                ps.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                conn.close();
            } catch (SQLException e) {
                /* ignored */ }
        }
        return listu;
    }

    public void Register(String name, String pass, String phone, String Dob, String isadmin, String student) {

        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([UserName]\n"
                + "           ,[PassWord]\n"
                + "           ,[Phone]\n"
                + "           ,[Dob]\n"
                + "           ,[Admin]\n"
                + "           ,[Student])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, pass);
            ps.setString(3, phone);
            ps.setString(4, Dob);
            ps.setString(5, isadmin);
            ps.setString(6, student);

//            đưa cái cần insert vào câu lệnh trên kia
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(RegisterDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                ps.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                conn.close();
            } catch (SQLException e) {
                /* ignored */ }
        }

    }

    
}
