/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.BlogFeedback;
import Model.Comment;
import Model.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tran dat
 */
public class blogFeedbackDao {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<BlogFeedback> getallblogFeedbackCommentbyBlogID(int blogID) {
        ArrayList<BlogFeedback> feedback = new ArrayList<>();
        try {
            String sql = "SELECT b.BfbID,b.BfbTitle,b.BfbDate,u.UserName,ISNULL(c.CmtID,0),c.CmtTitle,c.Cdate,uc.UserName\n"
                    + "                      FROM [BlogFeedback] b \n"
                    + "                  	left join [User] u on u.UserID = b.UserID\n"
                    + "                    	left join comments c on c.BfbID = b.BfbID\n"
                    + "                    	left join [User] uc on c.UserID = uc.UserID\n"
                    + "                      where b.BlogID = ?\n"
                    + "					  order by BfbID desc, c.cmtID desc";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, blogID);
            rs = ps.executeQuery();
            BlogFeedback current = new BlogFeedback();
            current.setBfbid(0);
            while (rs.next()) {
                int bfbID = rs.getInt(1);
                if (bfbID != current.getBfbid()) {
                    User ub = new User();
                    current = new BlogFeedback();

                    current.setBfbid(bfbID);
                    current.setBfbtitle(rs.getString(2));
                    current.setDate(rs.getDate(3));
                    ub.setUsername(rs.getString(4));
                    current.setUser(ub);
                    feedback.add(current);
                }
                int CmtID = rs.getInt(5);
                if (CmtID != 0) {
                    User uc = new User();
                    Comment c = new Comment();
                    c.setCmtid(CmtID);
                    c.setCmttitle(rs.getString(6));
                    c.setCdate(rs.getDate(7));
                    uc.setUsername(rs.getString(8));
                    c.setUser(uc);

                    current.getComment().add(c);
                    c.setBlogfeedback(current);
                }

            }

        } catch (Exception ex) {
            Logger.getLogger(blogFeedbackDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                ps.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                conn.close();
            } catch (SQLException e) {
                /* ignored */ }
        }
        return feedback;
    }

    public void insertBlogFeedBack(String blogID, String iduser, String bfbtitle, String bfbdate) {
        try {
            String sql = "INSERT INTO [dbo].[BlogFeedback]\n"
                    + "           ([BlogID]\n"
                    + "           ,[UserID]\n"
                    + "           ,[BfbTitle]\n"
                    + "           ,[BfbDate])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, blogID);
            ps.setString(2, iduser);
            ps.setString(3, bfbtitle);
            ps.setString(4, bfbdate);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(blogFeedbackDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                conn.close();
            } catch (SQLException e) {
                /* ignored */ }
        }

    }

    public void insertComment(String bfbid,String BlogID,  String iduser, String ctitle, String cdate) {
        try {
            String sql = "INSERT INTO [dbo].[comments]\n"
                    + "           ([BfbID]\n"
                    + "           ,[BlogID]\n"
                    + "           ,[UserID]\n"
                    + "           ,[CmtTitle]\n"
                    + "           ,[Cdate])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, bfbid);
            ps.setString(2, BlogID);
            ps.setString(3, iduser);
            ps.setString(4, ctitle);
            ps.setString(5, cdate);
            ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(blogFeedbackDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(blogFeedbackDao.class.getName()).log(Level.SEVERE, null, ex);
        }   
            try {
                ps.close();
            } catch (SQLException e) {
                /* ignored */ }
            try {
                conn.close();
            } catch (SQLException e) {
                /* ignored */ }
        
    }

}
