/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.BlogDao;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import Model.Blog;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tran dat
 */
@MultipartConfig(location="/addblog", fileSizeThreshold=1024*1024, maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
public class addblogController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet addblogController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet addblogController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String blogtitle = request.getParameter("blogtitle");
        String blogdescription = request.getParameter("blogdescription");
        Blog blog = new Blog();
        BlogDao bd = new BlogDao();
        Part filePart = request.getPart("photo");
        if(!"".equals(filePart.getSubmittedFileName()))
        {
            String imgpath= "images/";
            String filePath = getServletContext().getRealPath("") + File.separator + imgpath + blog.getBlogImage();
            File file = new File(filePath); 
            if (file.exists() && !file.isDirectory()) { 
               file.delete();
            } 
            String fileName =  Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); 
            InputStream inputStream = filePart.getInputStream();
            String uploadPath = getServletContext().getRealPath("") + File.separator + imgpath;
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            FileOutputStream outputStream = new FileOutputStream(uploadPath + 
            File.separator + fileName);
            int read = 0;
            final byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            inputStream.close();
            outputStream.close();
            blog.setBlogImage("images/"+filePart.getSubmittedFileName());
        }
        HttpSession session = request.getSession();
         int iduser =  (int) session.getAttribute("iduser") ;
         String date = java.time.LocalDate.now().toString();
         blog.setuID(iduser);
         blog.setBlogtitle(blogtitle);
         blog.setBlogdescription(blogdescription);
         bd.insertBlog(blog, date);
         response.sendRedirect("bloglist");
         
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
